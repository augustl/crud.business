.DEFAULT_GOAL := all
.PHONY: build
.PHONY: upload
.PHONY: invalidate
.PHONY: invalidate-cdn

invalidate:
	aws --profile crudbusiness cloudfront create-invalidation --distribution-id ECMFM3OA6N31P --paths "/*"

build:
	lein build-site

upload:
	aws --profile crudbusiness s3 sync dist/ s3://crud.business-web/ --delete

all: build upload