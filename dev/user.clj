(ns user
  (:require [crud-business.web :refer [app]]
            [ring.adapter.jetty]))

(defn start-server []
  (defonce server (ring.adapter.jetty/run-jetty #'app {:port 2782 :join? false}))
  (.start server))

(defn stop-server []
  (.stop server))
