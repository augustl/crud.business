provider "aws" {
  profile = "crudbusiness"
  region = "eu-north-1"
}

terraform {
  backend "s3" {
    bucket = "terraform-state-crud-business"
    key = "crudbusiness/terraform.tfstate"
    region = "eu-north-1"
    dynamodb_table = "terraform-locks"
  }
}

resource "aws_cloudfront_origin_access_identity" "crudbusiness_web_origin_access_identity" {
}


resource "aws_s3_bucket" "crudbusiness_web" {
  bucket = "crud.business-web"
  acl = "public-read"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::crud.business-web/*"
        }
    ]
}
EOF

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_website_configuration" "crudbusiness_web" {
  bucket = aws_s3_bucket.crudbusiness_web.bucket

  index_document {
    suffix = "index.html"
  }


}


resource "aws_s3_bucket" "crudbusiness_web_www" {
  bucket = "www.crud.business-web"
  acl = "public-read"
  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_website_configuration" "crudbusiness_web_www" {
  bucket = aws_s3_bucket.crudbusiness_web_www.bucket

  redirect_all_requests_to {
    host_name = "crud.business"
    protocol = "https"
  }
}


provider "aws" {
  region = "us-east-1"
  alias = "aws_us_east_1"
}

resource "aws_acm_certificate" "cert" {
  domain_name = "crud.business"
  validation_method = "DNS"
  subject_alternative_names = ["*.crud.business"]
  provider = aws.aws_us_east_1

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_route53_zone" "zone" {
  name = "crud.business"
  private_zone = false
}

resource "aws_route53_record" "cert_validation" {
  for_each = {
  for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
    name   = dvo.resource_record_name
    record = dvo.resource_record_value
    type   = dvo.resource_record_type
  }
  }

  allow_overwrite = true
  name = each.value.name
  records = [each.value.record]
  ttl = 60
  type = each.value.type
  zone_id = data.aws_route53_zone.zone.id
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in aws_route53_record.cert_validation : record.fqdn]
  provider = aws.aws_us_east_1
}

resource "aws_cloudfront_distribution" "crudbusiness_web" {
  enabled = true
  is_ipv6_enabled = true
  price_class = "PriceClass_200"
  http_version = "http2"

  origin {
    domain_name = aws_s3_bucket_website_configuration.crudbusiness_web.website_endpoint
    origin_id = "origin-${aws_s3_bucket.crudbusiness_web.id}"

    custom_origin_config {
      http_port = 80
      https_port = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols = ["TLSv1.2"]
    }
  }
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods = ["GET", "HEAD", "DELETE", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods = ["GET", "HEAD"]

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    min_ttl = "0"
    default_ttl = "300"  //3600
    max_ttl = "1200" //86400
    target_origin_id = "origin-${aws_s3_bucket.crudbusiness_web.id}"

    // This redirects any HTTP request to HTTPS. Security first!
    viewer_protocol_policy = "redirect-to-https"
    compress = true
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.cert.arn
    ssl_support_method = "sni-only"
  }

  aliases = ["crud.business"]

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "dns_web" {
  zone_id = data.aws_route53_zone.zone.id
  name    = "crud.business"
  type    = "A"

  alias {
    name = aws_cloudfront_distribution.crudbusiness_web.domain_name
    zone_id  = aws_cloudfront_distribution.crudbusiness_web.hosted_zone_id
    evaluate_target_health = true
  }

  depends_on = [aws_cloudfront_distribution.crudbusiness_web]
}

resource "aws_cloudfront_distribution" "crudbusiness_web_www" {
  enabled = true
  is_ipv6_enabled = true
  price_class = "PriceClass_200"
  http_version = "http2"

  origin {
    domain_name = aws_s3_bucket_website_configuration.crudbusiness_web_www.website_endpoint
    origin_id = "origin-${aws_s3_bucket.crudbusiness_web_www.id}"

    custom_origin_config {
      http_port = 80
      https_port = 443
      origin_protocol_policy = "http-only"
      origin_ssl_protocols = ["TLSv1.2"]
    }
  }

  // Must be blank for redirect to work
  default_root_object = ""

  default_cache_behavior {
    allowed_methods = ["GET", "HEAD", "DELETE", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods = ["GET", "HEAD"]

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    min_ttl = "0"
    default_ttl = "300"  //3600
    max_ttl = "1200" //86400
    target_origin_id = "origin-${aws_s3_bucket.crudbusiness_web_www.id}"

    // This redirects any HTTP request to HTTPS. Security first!
    viewer_protocol_policy = "redirect-to-https"
    compress = true
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.cert.arn
    ssl_support_method = "sni-only"
  }

  aliases = ["www.crud.business"]

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_route53_record" "dns_web_www" {
  zone_id = data.aws_route53_zone.zone.id
  name    = "www.crud.business"
  type    = "A"

  alias {
    name = aws_cloudfront_distribution.crudbusiness_web_www.domain_name
    zone_id  = aws_cloudfront_distribution.crudbusiness_web_www.hosted_zone_id
    evaluate_target_health = true
  }

  depends_on = [aws_cloudfront_distribution.crudbusiness_web_www]
}

output "cloudfront_distribution" {
  value = aws_cloudfront_distribution.crudbusiness_web.id
}