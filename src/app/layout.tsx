import "../../public/site.css"

export const metadata = {
  title: 'Oslo CRUD og business',
  description: 'August Lilleaas, selvstending softwareutvikler, forfatter og frilanskonsulent',
}

export default function RootLayout({children}: {children: React.ReactNode}) {
  return (
    <html lang="en">
      <head>
        <title>Oslo CRUD og business</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </head>
      <body>{children}</body>
    </html>
  )
}
