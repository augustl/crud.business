import Image from "next/image"
import { promises as fs } from 'fs'
import { parseEDNString } from 'edn-data'
import React from "react"

export default async function HomePage() {
    const cvData: any = parseEDNString(await fs.readFile(process.cwd() + "/resources/cvstuff.edn", "utf-8"), {mapAs: 'object', keywordAs: 'string'})

    return <div className={"flex flex-col gap-16"}>
        <div>
            <h1 className={"text-center"}>Oslo CRUD og Business AS</h1>
            <div className={"flex flex-col items-center"}>
                <Image src={"/august.jpg"} width={400} height={533.333} alt={""} />
            </div>
        </div>

        <div className={"dat-box"}>
            <h2>Jeg heter August Lilleaas</h2>
            <div className={"dat-box-body"}>
                <p>Jeg er en softwareutvikler og selvstendig IT-konsulent med 20 års erfaring.</p>
                <p>Jeg har <a href={"https://link.springer.com/book/10.1007/978-1-4842-9057-6"}>skrevet bok om Kotlin</a></p>
                <p>
                    Jeg har holdt foredrag både
                    {" "}
                    <a title={"Foredrag, Kotlin Conf 2018 (Amsterdam, Nederland)"} href={"https://www.youtube.com/watch?v=hicQvxdKvnc&list=PLQ176FUIyIUbVvFMqDc2jhxS-t562uytr&index=40"}>her (KotlinConf i Amsterdam)</a>
                    {" "}
                    og
                    {" "}
                    <a title={"https://vimeo.com/74406482"} href={"Foredrag, JavaZone (Oslo) 2013"}>der (JavaZone i Oslo)</a>
                    .
                </p>
                <p>
                    Jeg fikser bugs i open source-prosjekter når anledningen byr seg, f.eks
                    {" "}
                    <a href={"https://github.com/facebook/react-native/pull/19814"} title={"Bugfix i React Native: Allow ReactActivityDelegate subclass to overrride mainComponentName"}>React Native</a>
                    ,
                    {" "}
                    <a href={"https://github.com/immutable-js/immutable-js/pull/1656"} title={"Bugfix i Immutable.JS: Fix build error caused by flatmap-stream-0.1.1 yarn registry removal"}>Immutable.JS</a>
                    {" "}
                    og
                    {" "}
                    <a href={"https://github.com/seratch/kotliquery/pull/57"} title="Enhancement i Kotliquery: Make Session.transaction inline">Kotliquery</a>
                    . Og jeg har
                    {" "}
                    <a href={"http://lkml.iu.edu/hypermail/linux/kernel/1105.3/02740.html"}>en commit</a>
                    {" "}
                    i Linux-kernelen 🥳
                </p>
                <p>
                    Jeg blogger i ny og ne, og har bl.a. skrevet om at
                    {" "}
                    <a href={"https://augustl.com/blog/2018/datomic_look_at_all_the_things_i_am_not_doing/"}>Datomic er rått</a>
                    , laget fancy animasjoner for å
                    {" "}
                    <a href={"https://www.kodemaker.no/blogg/2019-08-public-og-private-keys/"}>forklare asymmetrisk kryptografi</a>
                    , og gått i dybden på
                    {" "}
                    <a href={"https://www.kodemaker.no/blogg/2020-11-google-docs-og-vscode-tekst/"}>hvordan Google Docs og VSCode rendrer tekst</a>
                    .
                </p>
                <p>Tidligere har jeg jobbet som IT-konsulent i Kodemaker, jeg har laget apper i Shortcut, og jeg betalte mitt eget førerkort i 2005 ved å lage websider for 4000 kroner stykket.</p>

                <dl title={"Kontaktinfo"}>
                    <dt>Telefon</dt>
                    <dd><a href={`tel:${cvData["cv/phone"]}`}>{cvData["cv/phone"]}</a></dd>

                    <dt>E-post</dt>
                    <dd><a href={`mailto:${cvData["cv/email"]}`}>{cvData["cv/email"]}</a></dd>

                    <dt>Orgnr.</dt>
                    <dd><a href={`https://w2.brreg.no/enhet/sok/detalj.jsp?orgnr=${cvData["cv/orgnr"].replace(/\s/g, "")}`}>{cvData["cv/orgnr"]}</a></dd>
                </dl>
            </div>
        </div>

        <div className={"dat-box"}>
            <h2>Bok - Pro Kotlin Web Apps from Scratch</h2>
            <div className={"dat-box-body"}>
                <p className={"italic text-gray-500"}>Apress, 2023</p>

                <p>Jeg har skrevet en bok! Den handler om Kotlin, og å skrive web-apper uten å bruke svære rammeverk.</p>
                <p>Noe av det kuleste i boka, er kapitlene om hvordan man kan bruke Spring Security og Spring Context, uten å bruke hele Spring-rammeverket. Hvis man har lyst til å skrive moderne web-apper uten rammeverk, men jobber i en organisasjon som bruker Spring utstrakt, kan man f.eks helt fint gjenbruke plugins til Spring Security, uten å måtte dra inn hele rammeverket, og i en kontekst hvor man kan skrive moderne og smooth Kotlin-kode.</p>

                <p className={"text-center my-10"}>
                    <a tabIndex={-1} href={"https://www.amazon.com/Pro-Kotlin-Apps-Scratch-Production-ready/dp/1484290569"} title={"Kjøp boka Pro Kotlin Web Apps from Scratch"}>
                        <Image src={"/kotlin-book-pro-kotlin-web-apps-from-scratch-cover.jpg"} alt={""} width={400} height={570} />
                    </a>
                </p>

                <p>
                    Du kan
                    {" "}
                    <a href={"https://www.amazon.com/Pro-Kotlin-Apps-Scratch-Production-ready/dp/1484290569"}>kjøpe boka i papirformat eller Kindle på Amazon</a>
                    . Du kan også
                    {" "}
                    <a href={"https://www.adlibris.com/no/bok/pro-kotlin-web-apps-from-scratch-9781484290569"}>kjøpe boka hos Adlibris i papirformat</a>
                    {" "}
                    og mange andre norske bokhandler. Forlaget selger også boka direkte, og du kan
                    {" "}
                    <a href={"https://link.springer.com/book/10.1007/978-1-4842-9057-6"}>kjøpe boka i papirformat eller PDF, ePub og andre formater</a>
                    {" "}
                    hos dem.
                </p>

                <div className={"flex flex-col justify-center items-center gap-2"}>
                    <a className={"extremely-sexy-button"} href={"https://www.amazon.com/Pro-Kotlin-Apps-Scratch-Production-ready/dp/1484290569"} title={"Kjøp boka Pro Kotlin Web Apps from Scratch fra Amazon i USA"}>
                        Kjøp boka (Amazon)
                    </a>
                    <a className={"extremely-sexy-button"} href={"https://www.adlibris.com/no/bok/pro-kotlin-web-apps-from-scratch-9781484290569"} title={"Kjøp boka Pro Kotlin Web Apps from Scratch fra Adlibris i Norge"}>
                        Kjøp boka (Adlibris, Norge)
                    </a>
                    <a className={"extremely-sexy-button"} href={"https://link.springer.com/book/10.1007/978-1-4842-9057-6"} title={"Kjøp boka Pro Kotlin Web Apps from Scratch fra forlaget i digitale formater som PDF og ePub"}>
                        Kjøp boka (PDF, ePub, ...)
                    </a>
                </div>
            </div>
        </div>

        <div>
            <h2>Presentasjoner</h2>
            <p>Jeg liker å prate. Her er et knippe presentasjoner jeg har holdt på konferanser o.l. rundt om i verden.</p>

            <div className={"default-2x-grid mob-1x"}>
                {cvData["cv/presentations"].map((presentation: any) => {
                    const description = presentation["presentation/description"]
                    return <div className={"dat-box"} key={presentation["presentation/url"]}>
                        <h3 className={"lax alt"}>{presentation["presentation/place"]}, {presentation["presentation/date"]}</h3>
                        <div className={"dat-box-body"}>
                            <p>{presentation["presentation/name"]}</p>
                            {description && <p><em>{description}</em></p>}
                            <div className={"flex justify-center"}>
                                <a
                                    href={presentation["presentation/url"]}
                                    className={"extremely-sexy-button"}
                                    target={"_blank"}
                                    title={`Se presentasjonen ${presentation["presentation/name"]} fra ${presentation["presentation/place"]}, ${presentation["presentation/date"]}`}
                                >
                                    Se presentasjon
                                </a>
                            </div>
                        </div>
                    </div>
                })}
            </div>
        </div>

        <h2 className={"mt-16"}>Prosjekter</h2>

        <div className={"default-2x-grid mob-1x"}>
            {cvData["cv/projects"].map((project: any) => {
                const customer = project["project/customer"]
                return <div key={JSON.stringify(project)}>
                    <h3 className={"lax alt"}>{project["project/summary"]}</h3>
                    <p className={"supermeta"}>{customer && `${customer}, `}<DurationFromTo from={project["project/start"]} to={project["project/end"]} /></p>
                    <p>{project["project/description"]}</p>
                    <p className={"othermeta"}>
                        {project["project/tech"]
                            .map((tech: any) => {
                                const techNames = cvData["cv/tech-names"]
                                const namedTech = techNames[tech]
                                if (namedTech) {
                                    return namedTech
                                }

                                return tech
                                    .split(/-/)
                                    .map(capitalize)
                                    .join(" ")
                            })
                            .join(", ")}
                    </p>
                </div>
            })}
        </div>

        <h2 className={"mt-16"}>Bloggposter</h2>

        <div className={"default-2x-grid mob-1x"}>
            {cvData["cv/blog-posts"].map((blogPost: any) => {
                const domain = getDomainName(blogPost["blog-post/url"])
                return <div className={"dat-box"} key={JSON.stringify(blogPost)}>
                    <h3 className={"lax alt"}>{blogPost["blog-post/title"]}</h3>
                    <div className={"dat-box-body"}>
                        <p>{blogPost["blog-post/description"]}</p>
                        <div className={"flex justify-center"}>
                            <a
                                href={blogPost["blog-post/url"]}
                                className={"extremely-sexy-button"}
                                target={"_blank"}
                                title={`Les bloggposten  ${blogPost["blog-post/title"]} på ${domain}`}
                            >
                                Les bloggpost på {domain}
                            </a>
                        </div>
                    </div>
                </div>
            })}
        </div>
    </div>
}

const getDomainName = (str: string): string => {
    return new URL(str).hostname
}

const capitalize = (str: string): string => {
    const letters = []
    for (const char of str) {
        letters.push(char)
    }

    if (letters.length === 0) {
        return ""
    }

    return `${letters[0].toUpperCase()}${letters.slice(1).join("")}`
}

const durationToText = (duration: string): string => {
    const match =  duration.match(/^(\d\d\d\d)-?(\d\d)?-?(\d\d)?/)
    if (!match) {
        return ""
    }

    const [_, y, m, d] = match

    if (y && m && d) {
        return `${d}.${m}.${y}`
    }

    if (y && m) {
        return `${m}.${y}`
    }

    return y
}

const DurationFromTo: React.FC<{from: string, to: string | undefined | null}> = ({from, to}) => {
    return `${durationToText(from)} - ${to ? durationToText(to) : ""}`
}